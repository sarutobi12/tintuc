@extends('home.master2')
@section('content')

<!-- Menu wrapper end -->
<div class="trending-light d-md-block d-lg-block d-none">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="trending-title">Tin mới</h3>
                <div id="trending-slide" class="owl-carousel owl-theme trending-slide">
                	@foreach($khuyenmai as $item_km)
                    <div class="item">
                        <div class="post-content">
                            <h2 class="post-title title-small">
                                <a href="{{url('/chi-tiet/'.$item_km->slug)}}">{{ $item_km->newsname}}</a>
                            </h2>
                        </div>
                        <!-- Post content end -->
                    </div>
                    @endforeach
                    <!-- Item 1 end -->
                </div>
                <!-- Carousel end -->
            </div>
            <!-- Col end -->
        </div>
        <!--/ Row end -->
    </div>
    <!--/ Container end -->
</div>
<!--/ Trending end -->
<section class="featured-post-area no-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 pad-r">
                <div id="featured-slider" class="owl-carousel owl-theme featured-slider">
                    <div class="item" style="background-image:url(public/frontend/images/news/lifestyle/health5.jpg)">
                        <div class="featured-post">
                            <div class="post-content">
                                <a class="post-cat" href="#">Health</a>
                                <h2 class="post-title title-extra-large">
                                    <a href="#">Netcix cuts out the chill with an integrated personal trainer on running</a>
                                </h2>
                                <span class="post-date">March 16, 2017</span>
                            </div>
                        </div>
                        <!--/ Featured post end -->
                    </div>
                    <!-- Item 1 end -->
                    <div class="item" style="background-image:url(public/frontend/images/news/tech/gadget2.jpg)">
                        <div class="featured-post">
                            <div class="post-content">
                                <a class="post-cat" href="#">Gadget</a>
                                <h2 class="post-title title-extra-large">
                                    <a href="#">Samsung Gear S3 review: A whimper, when smartwatches need a bang</a>
                                </h2>
                                <span class="post-date">March 16, 2017</span>
                            </div>
                        </div>
                        <!--/ Featured post end -->
                    </div>
                    <!-- Item 2 end -->
                    <div class="item" style="background-image:url(public/frontend/images/news/lifestyle/travel5.jpg)">
                        <div class="featured-post">
                            <div class="post-content">
                                <a class="post-cat" href="#">Travel</a>
                                <h2 class="post-title title-extra-large">
                                    <a href="#">Hynopedia helps female travelers find health care in Maldivs</a>
                                </h2>
                                <span class="post-date">March 16, 2017</span>
                            </div>
                        </div>
                        <!--/ Featured post end -->
                    </div>
                    <!-- Item 3 end -->
                </div>
                <!-- Featured owl carousel end-->
            </div>
            <!-- Col 6 end -->
            <div class="col-lg-6 col-md-12 pad-l">
                <div class="row">
                    <div class="col-md-6 pad-r-small">
                        <div class="post-overaly-style contentTop fourNewsboxTop clearfix">
                            <div class="post-thumb">
                                <a href="#"><img class="img-fluid" src="public/frontend/images/news/tech/gadget2.jpg" alt="" /></a>
                            </div>
                            <div class="post-content">
                                <a class="post-cat" href="#">Gadgets</a>
                                <h2 class="post-title title-medium">
                                    <a href="#">Samsung Gear S3 review: A whimper, when…</a>
                                </h2>
                            </div>
                            <!-- Post content end -->
                        </div>
                        <!-- Post Overaly end -->
                    </div>
                    <!-- Col end -->
                    <div class="col-md-6 pad-l-small">
                        <div class="post-overaly-style contentTop fourNewsboxTop clearfix">
                            <div class="post-thumb">
                                <a href="#"><img class="img-fluid" src="public/frontend/images/news/tech/game1.jpg" alt="" /></a>
                            </div>
                            <div class="post-content">
                                <a class="post-cat" href="#">Games</a>
                                <h2 class="post-title title-medium">
                                    <a href="#">Historical heroes and robot dinosaurs:...</a>
                                </h2>
                            </div>
                            <!-- Post content end -->
                        </div>
                        <!-- Post Overaly end -->
                    </div>
                    <!-- Col end -->
                    <div class="col-md-6 pad-r-small">
                        <div class="post-overaly-style contentTop fourNewsbox clearfix">
                            <div class="post-thumb">
                                <a href="#"><img class="img-fluid" src="public/frontend/images/news/lifestyle/travel2.jpg" alt="" /></a>
                            </div>
                            <div class="post-content">
                                <a class="post-cat" href="#">Travel</a>
                                <h2 class="post-title title-medium">
                                    <a href="#">Early tourists choices to the sea of Maldiv…</a>
                                </h2>
                            </div>
                            <!-- Post content end -->
                        </div>
                        <!-- Post Overaly end -->
                    </div>
                    <!-- Col end -->
                    <div class="col-md-6 pad-l-small">
                        <div class="post-overaly-style contentTop fourNewsbox clearfix">
                            <div class="post-thumb">
                                <a href="#"><img class="img-fluid" src="public/frontend/images/news/lifestyle/health1.jpg" alt="" /></a>
                            </div>
                            <div class="post-content">
                                <a class="post-cat" href="#">Health</a>
                                <h2 class="post-title title-medium">
                                    <a href="#">That wearable on your wrist could soon...</a>
                                </h2>
                            </div>
                            <!-- Post content end -->
                        </div>
                        <!-- Post Overaly end -->
                    </div>
                    <!-- Col end -->
                </div>
                <!-- Row end -->
            </div>
            <!-- Col 6 end -->
        </div>
        <!-- Row end -->
    </div>
    <!-- Container end -->
</section>
<!-- Trending post end -->
<section class="block-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <!--- Featured Tab startet -->
                <?php $index_count = 0; $ads = 0;?>
                @foreach($modnews as $index_mod)
                <div class="featured-tab color-blue">
                    <h3 class="block-title"><span>{{ $index_mod->modname }}</span></h3>
                    <ul class="nav nav-tabs">
                    	@foreach($index_mod->listnews as $itemlist) 
                        <li>
                            <a class="active animated fadeIn" href="{{url('/loai-tin2/'.$itemlist->slug)}}">
                            <span class="tab-head">
                            <span class="tab-text-title">{{$itemlist->listname}}</span>					
                            </span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    <?php 
						$item = $index_mod->news_in_mod($index_mod->id);
						$hot = $item->shift();								
					 ?>
                    <div class="tab-content">
                        <div class="tab-pane active animated fadeInRight" id="tab_a">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="post-block-style clearfix">
                                        <div class="post-thumb">
                                            <a href="{{url('chi-tiet/'.$hot['slug'])}}">
                                            <img class="img-fluid" src="{{url('public/img/news/300x300/'.$hot['newimg'])}}" alt="" />
                                            </a>
                                        </div>
                                        <a class="post-cat" href="{{url('chi-tiet2/'.$hot['slug'])}}"><i class="fa fa-comment-o"></i></a>
                                        <div class="post-content">
                                            <h2 class="post-title">
                                                <a href="{{url('chi-tiet2/'.$hot['slug'])}}">{{$hot['newsname']}}</a>
                                            </h2>
                                            <div class="post-meta">
                                                <span class="post-author"><a href="#">Admin</a></span>
                                                <span class="post-date">03 tháng 2 , 2019</span>
                                            </div>
                                            <p>{!! $hot['newintro'] !!}</p>
                                        </div>
                                        <!-- Post content end -->
                                    </div>
                                    <!-- Post Block style end -->
                                </div>
                                <!-- Col end -->
                                <div class="col-md-6">
                                    <div class="list-post-block">
                                        <ul class="list-post">
                                        	@foreach($item as $m_item)
                                            <li class="clearfix">
                                                <div class="post-block-style post-float clearfix">
                                                    <div class="post-thumb">
                                                        <a href="{{url('/chi-tiet/'.$m_item->slug)}}">
                                                        <img class="img-fluid" src="{{url('public/img/news/300x300/'.$m_item['newimg'])}}" alt="" />
                                                        </a>
                                                    </div>
                                                    <!-- Post thumb end -->
                                                    <div class="post-content">
                                                        <h2 class="post-title title-small">
                                                            <a href="{{url('/chi-tiet/'.$m_item->slug)}}">{{$m_item->newsname}}</a>
                                                        </h2>
                                                        <div class="post-meta">
                                                            <span class="post-date">03 tháng 2, 2019</span>
                                                        </div>
                                                    </div>
                                                    <!-- Post content end -->
                                                </div>
                                                <!-- Post block style end -->
                                            </li>
                                           	@endforeach
                                            <!-- Li 4 end -->
                                        </ul>
                                        <!-- List post end -->
                                    </div>
                                    <!-- List post block end -->
                                </div>
                                <!-- List post Col end -->
                            </div>
                            <!-- Tab pane Row 1 end -->
                        </div>
                        <!-- Tab pane 1 end -->
                        <!-- Tab pane 3 end -->	
                    </div>
                    <!-- tab content -->
                </div>
                <hr>
                @endforeach
                <!-- Technology Tab end -->
               
                <!-- Block Lifestyle end -->
               
                <!--- Latest news end -->
            </div>
            <!-- Content Col end -->
            @include('home.sitebar_right2')
        </div>
        <!-- Row end -->
    </div>
    <!-- Container end -->
</section>
<!-- First block end -->
<section class="ad-content-area text-center no-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img class="img-fluid" src="public/frontend/images/banner-ads/ad-content-one.jpg" alt="" />
            </div>
            <!-- Col end -->
        </div>
        <!-- Row end -->
    </div>
    <!-- Container end -->
</section>
<!-- Ad content top end -->
<!-- <section class="block-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <div class="block color-dark-blue">
                    <h3 class="block-title"><span>Travel</span></h3>
                    <div class="post-overaly-style clearfix">
                        <div class="post-thumb">
                            <a href="#">
                            <img class="img-fluid" src="public/frontend/images/news/lifestyle/travel1.jpg" alt="" />
                            </a>
                        </div>
                        <div class="post-content">
                            <h2 class="post-title">
                                <a href="#">10 Hdrenaline fuelled activities that will chase the…</a>
                            </h2>
                            <div class="post-meta">
                                <span class="post-date">Mar 03, 2017</span>
                            </div>
                        </div>
                        Post content end
                    </div>
                    Post Overaly Article end
                    <div class="list-post-block">
                        <ul class="list-post">
                            <li class="clearfix">
                                <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="#">
                                        <img class="img-fluid" src="public/frontend/images/news/lifestyle/travel2.jpg" alt="" />
                                        </a>
                                    </div>
                                    Post thumb end
                                    <div class="post-content">
                                        <h2 class="post-title title-small">
                                            <a href="#">Early tourists choices to the sea of Maldives in fancy dress…</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="post-date">Mar 13, 2017</span>
                                        </div>
                                    </div>
                                    Post content end
                                </div>
                                Post block style end
                            </li>
                            Li 1 end
                            <li class="clearfix">
                                <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="#">
                                        <img class="img-fluid" src="public/frontend/images/news/lifestyle/travel3.jpg" alt="" />
                                        </a>
                                    </div>
                                    Post thumb end
                                    <div class="post-content">
                                        <h2 class="post-title title-small">
                                            <a href="#">This Aeroplane that looks like a butt is the largest aircraf…</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="post-date">Jan 11, 2017</span>
                                        </div>
                                    </div>
                                    Post content end
                                </div>
                                Post block style end
                            </li>
                            Li 2 end
                            <li class="clearfix">
                                <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="#">
                                        <img class="img-fluid" src="public/frontend/images/news/lifestyle/travel4.jpg" alt="" />
                                        </a>
                                    </div>
                                    Post thumb end
                                    <div class="post-content">
                                        <h2 class="post-title title-small">
                                            <a href="#">19 incredible photos from Disney's 'Star Wars' cruise algore</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="post-date">Feb 19, 2017</span>
                                        </div>
                                    </div>
                                    Post content end
                                </div>
                                Post block style end
                            </li>
                            Li 3 end
                        </ul>
                        List post end
                    </div>
                    List post block end
                </div>
                Block end
            </div>
            Travel Col end
            <div class="col-lg-4 col-md-12">
                <div class="block color-aqua">
                    <h3 class="block-title"><span>Gadgets</span></h3>
                    <div class="post-overaly-style clearfix">
                        <div class="post-thumb">
                            <a href="#">
                            <img class="img-fluid" src="public/frontend/images/news/tech/gadget1.jpg" alt="" />
                            </a>
                        </div>
                        <div class="post-content">
                            <h2 class="post-title">
                                <a href="#">The best MacBook Pro alternatives in 2017 for Apple users</a>
                            </h2>
                            <div class="post-meta">
                                <span class="post-date">Mar 03, 2017</span>
                            </div>
                        </div>
                        Post content end
                    </div>
                    Post Overaly Article end
                    <div class="list-post-block">
                        <ul class="list-post">
                            <li class="clearfix">
                                <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="#">
                                        <img class="img-fluid" src="public/frontend/images/news/tech/gadget2.jpg" alt="" />
                                        </a>
                                    </div>
                                    Post thumb end
                                    <div class="post-content">
                                        <h2 class="post-title title-small">
                                            <a href="#">Samsung Gear S3 review: A whimper, when smartwatches need...</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="post-date">Jan 13, 2017</span>
                                        </div>
                                    </div>
                                    Post content end
                                </div>
                                Post block style end
                            </li>
                            Li 1 end
                            <li class="clearfix">
                                <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="#">
                                        <img class="img-fluid" src="public/frontend/images/news/tech/gadget3.jpg" alt="" />
                                        </a>
                                    </div>
                                    Post thumb end
                                    <div class="post-content">
                                        <h2 class="post-title title-small">
                                            <a href="#">Panasonic's new Sumix CH7 an ultra portable filmmaker's drea…</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="post-date">Mar 11, 2017</span>
                                        </div>
                                    </div>
                                    Post content end
                                </div>
                                Post block style end
                            </li>
                            Li 2 end
                            <li class="clearfix">
                                <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="#">
                                        <img class="img-fluid" src="public/frontend/images/news/tech/gadget4.jpg" alt="" />
                                        </a>
                                    </div>
                                    Post thumb end
                                    <div class="post-content">
                                        <h2 class="post-title title-small">
                                            <a href="#">Soaring through Southern Patagonia with the Premium Byrd dro…</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="post-date">Feb 19, 2017</span>
                                        </div>
                                    </div>
                                    Post content end
                                </div>
                                Post block style end
                            </li>
                            Li 3 end
                        </ul>
                        List post end
                    </div>
                    List post block end
                </div>
                Block end
            </div>
            Gadget Col end
            <div class="col-lg-4 col-md-12">
                <div class="block color-violet">
                    <h3 class="block-title"><span>Health</span></h3>
                    <div class="post-overaly-style clearfix">
                        <div class="post-thumb">
                            <a href="#">
                            <img class="img-fluid" src="public/frontend/images/news/lifestyle/health1.jpg" alt="" />
                            </a>
                        </div>
                        <div class="post-content">
                            <h2 class="post-title">
                                <a href="#">That wearable on your wrist could soon track your health as …</a>
                            </h2>
                            <div class="post-meta">
                                <span class="post-date">Mar 03, 2017</span>
                            </div>
                        </div>
                        Post content end
                    </div>
                    Post Overaly Article end
                    <div class="list-post-block">
                        <ul class="list-post">
                            <li class="clearfix">
                                <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="#">
                                        <img class="img-fluid" src="public/frontend/images/news/lifestyle/health2.jpg" alt="" />
                                        </a>
                                    </div>
                                    Post thumb end
                                    <div class="post-content">
                                        <h2 class="post-title title-small">
                                            <a href="#">Can't shed those Gym? The problem might be in your health</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="post-date">Mar 13, 2017</span>
                                        </div>
                                    </div>
                                    Post content end
                                </div>
                                Post block style end
                            </li>
                            Li 1 end
                            <li class="clearfix">
                                <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="#">
                                        <img class="img-fluid" src="public/frontend/images/news/lifestyle/health3.jpg" alt="" />
                                        </a>
                                    </div>
                                    Post thumb end
                                    <div class="post-content">
                                        <h2 class="post-title title-small">
                                            <a href="#">Deleting fears from the brain means you might never need to …</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="post-date">Jan 11, 2017</span>
                                        </div>
                                    </div>
                                    Post content end
                                </div>
                                Post block style end
                            </li>
                            Li 2 end
                            <li class="clearfix">
                                <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="#">
                                        <img class="img-fluid" src="public/frontend/images/news/lifestyle/health4.jpg" alt="" />
                                        </a>
                                    </div>
                                    Post thumb end
                                    <div class="post-content">
                                        <h2 class="post-title title-small">
                                            <a href="#">Smart packs parking sensor tech and beeps when collisions</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="post-date">Feb 19, 2017</span>
                                        </div>
                                    </div>
                                    Post content end
                                </div>
                                Post block style end
                            </li>
                            Li 3 end
                        </ul>
                        List post end
                    </div>
                    List post block end
                </div>
                Block end
            </div>
            Health Col end
        </div>
        Row end
    </div>
    Container end
</section> -->
<!-- 2nd block end -->
<!-- <section class="block-wrapper video-block">
    <div class="container">
        <div class="row">
            <div class="video-tab clearfix">
                <h2 class="video-tab-title">Watch Now</h2>
                <div class="row">
                    <div class="col-lg-7 pad-r-0">
                        <div class="tab-content">
                            <div class="tab-pane active animated fadeIn" id="video1">
                                <div class="post-overaly-style clearfix">
                                    <div class="post-thumb">
                                        <img class="img-fluid" src="public/frontend/images/news/video/video4.jpg" alt="" />
                                        <a class="popup" href="https://www.youtube.com/embed/XhveHKJWnOQ?autoplay=1&amp;loop=1">
                                            <div class="video-icon">
                                                <i class="fa fa-play"></i>
                                            </div>
                                        </a>
                                    </div>
                                    Post thumb end
                                    <div class="post-content">
                                        <a class="post-cat" href="#">Video</a>
                                        <h2 class="post-title">
                                            <a href="#">Is Running Good for You, Health Benefits of Morning Running</a>
                                        </h2>
                                    </div>
                                    Post content end
                                </div>
                                Post Overaly Article end
                            </div>
                            Tab pane 1 end
                            <div class="tab-pane animated fadeIn" id="video2">
                                <div class="post-overaly-style clearfix">
                                    <div class="post-thumb">
                                        <img class="img-fluid" src="public/frontend/images/news/video/video3.jpg" alt="" />
                                        <a class="popup" href="https://www.youtube.com/embed/wJF5NXygL4k?autoplay=1&amp;loop=1">
                                            <div class="video-icon">
                                                <i class="fa fa-play"></i>
                                            </div>
                                        </a>
                                    </div>
                                    Post thumb end
                                    <div class="post-content">
                                        <a class="post-cat" href="#">Video</a>
                                        <h2 class="post-title title-medium">
                                            <a href="#">Breeze through 17 locations in Europe in this breathtaking video</a>
                                        </h2>
                                    </div>
                                    Post content end
                                </div>
                                Post Overaly Article 2 end
                            </div>
                            Tab pane 2 end
                            <div class="tab-pane animated fadeIn" id="video3">
                                <div class="post-overaly-style clearfix">
                                    <div class="post-thumb">
                                        <img class="img-fluid" src="public/frontend/images/news/video/video2.jpg" alt="" />
                                        <a class="popup" href="https://www.youtube.com/embed/DQNDcxRo-2M?autoplay=1&amp;loop=1">
                                            <div class="video-icon">
                                                <i class="fa fa-play"></i>
                                            </div>
                                        </a>
                                    </div>
                                    Post thumb end
                                    <div class="post-content">
                                        <a class="post-cat" href="#">Video</a>
                                        <h2 class="post-title title-medium">
                                            <a href="#">TG G6 will have dual 13-megapixel cameras on the back</a>
                                        </h2>
                                    </div>
                                    Post content end
                                </div>
                                Post Overaly Article 2 end
                            </div>
                            Tab pane 2 end
                        </div>
                        Tab content end
                    </div>
                    Tab col end
                    <div class="col-lg-5 pad-l-0">
                        <ul class="nav nav-tabs">
                            <li class="nav-item active">
                                <a class="nav-link animated fadeIn" href="#video1" data-toggle="tab">
                                    <div class="post-thumb">
                                        <img class="img-fluid" src="public/frontend/images/news/video/video4.jpg" alt="" />
                                    </div>
                                    Post thumb end
                                    <h3>Is Running Good for You, Health Benefits of Morning Running</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link animated fadeIn" href="#video2" data-toggle="tab">
                                    <div class="post-thumb">
                                        <img class="img-fluid" src="public/frontend/images/news/video/video3.jpg" alt="" />
                                    </div>
                                    Post thumb end
                                    <h3>Breeze through 17 locations in Europe in this breathtaking video</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link animated fadeIn" href="#video3" data-toggle="tab">
                                    <div class="post-thumb">
                                        <img class="img-fluid" src="public/frontend/images/news/video/video2.jpg" alt="" />
                                    </div>
                                    Post thumb end
                                    <h3>TG G6 will have dual 13-megapixel cameras on the back</h3>
                                </a>
                            </li>
                        </ul>
                    </div>
                    Tab nav col end
                </div>
            </div>
            Video tab end
        </div>
        Row end
    </div>
    Container end
</section> -->
<!-- Video block end -->
<!-- <section class="block-wrapper p-bottom-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="more-news block color-default">
                    <h3 class="block-title"><span>More News</span></h3>
                    <div id="more-news-slide" class="owl-carousel owl-theme more-news-slide">
                        <div class="item">
                            <div class="post-block-style post-float-half clearfix">
                                <div class="post-thumb">
                                    <a href="#">
                                    <img class="img-fluid" src="public/frontend/images/news/video/video1.jpg" alt="" />
                                    </a>
                                </div>
                                <a class="post-cat" href="#">Video</a>
                                <div class="post-content">
                                    <h2 class="post-title">
                                        <a href="#">KJerry's will sell food cream that tastes like your favorite video</a>
                                    </h2>
                                    <div class="post-meta">
                                        <span class="post-author"><a href="#">John Doe</a></span>
                                        <span class="post-date">Mar 29, 2017</span>
                                    </div>
                                    <p>Lumbersexual meh sustainable Thundercats meditation kogi. Tilde Pitchfork vegan, gentrify minim elit semiotics non messenger bag Austin which roasted ...</p>
                                </div>
                                Post content end
                            </div>
                            Post Block style 1 end
                            <div class="gap-30"></div>
                            <div class="post-block-style post-float-half clearfix">
                                <div class="post-thumb">
                                    <a href="#">
                                    <img class="img-fluid" src="public/frontend/images/news/tech/game5.jpg" alt="" />
                                    </a>
                                </div>
                                <a class="post-cat" href="#">Games</a>
                                <div class="post-content">
                                    <h2 class="post-title">
                                        <a href="#">Oazer and Lacon bring eSport expertise to new PS4 controllers</a>
                                    </h2>
                                    <div class="post-meta">
                                        <span class="post-author"><a href="#">John Doe</a></span>
                                        <span class="post-date">Mar 27, 2017</span>
                                    </div>
                                    <p>Pityful a rethoric question ran over her cheek When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of he...</p>
                                </div>
                                Post content end
                            </div>
                            Post Block style 2 end
                            <div class="gap-30"></div>
                            <div class="post-block-style post-float-half clearfix">
                                <div class="post-thumb">
                                    <a href="#">
                                    <img class="img-fluid" src="public/frontend/images/news/tech/game4.jpg" alt="" />
                                    </a>
                                </div>
                                <a class="post-cat" href="#">Games</a>
                                <div class="post-content">
                                    <h2 class="post-title">
                                        <a href="#">Super Tario Run isn’t groundbreaking, but it has Mintendo charm</a>
                                    </h2>
                                    <div class="post-meta">
                                        <span class="post-author"><a href="#">John Doe</a></span>
                                        <span class="post-date">Feb 24, 2017</span>
                                    </div>
                                    <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and ...</p>
                                </div>
                                Post content end
                            </div>
                            Post Block style 3 end
                            <div class="gap-30"></div>
                            <div class="post-block-style post-float-half clearfix">
                                <div class="post-thumb">
                                    <a href="#">
                                    <img class="img-fluid" src="public/frontend/images/news/tech/robot5.jpg" alt="" />
                                    </a>
                                </div>
                                <a class="post-cat" href="#">Robotics</a>
                                <div class="post-content">
                                    <h2 class="post-title">
                                        <a href="#">Robots in hospitals can be quite handy to navigate around the ho…</a>
                                    </h2>
                                    <div class="post-meta">
                                        <span class="post-author"><a href="#">John Doe</a></span>
                                        <span class="post-date">Feb 24, 2017</span>
                                    </div>
                                    <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and ...</p>
                                </div>
                                Post content end
                            </div>
                            Post Block style 4 end
                        </div>
                        Item 1 end
                        <div class="item">
                            <div class="post-block-style post-float-half clearfix">
                                <div class="post-thumb">
                                    <a href="#">
                                    <img class="img-fluid" src="public/frontend/images/news/video/video2.jpg" alt="" />
                                    </a>
                                </div>
                                <a class="post-cat" href="#">Video</a>
                                <div class="post-content">
                                    <h2 class="post-title">
                                        <a href="#">TG G6 will have dual 13-megapixel cameras on the back</a>
                                    </h2>
                                    <div class="post-meta">
                                        <span class="post-author"><a href="#">John Doe</a></span>
                                        <span class="post-date">Mar 29, 2017</span>
                                    </div>
                                    <p>Lumbersexual meh sustainable Thundercats meditation kogi. Tilde Pitchfork vegan, gentrify minim elit semiotics non messenger bag Austin which roasted ...</p>
                                </div>
                                Post content end
                            </div>
                            Post Block style 5 end
                            <div class="gap-30"></div>
                            <div class="post-block-style post-float-half clearfix">
                                <div class="post-thumb">
                                    <a href="#">
                                    <img class="img-fluid" src="public/frontend/images/news/video/video3.jpg" alt="" />
                                    </a>
                                </div>
                                <a class="post-cat" href="#">Video</a>
                                <div class="post-content">
                                    <h2 class="post-title">
                                        <a href="#">Breeze through 17 locations in Europe in this breathtaking v…</a>
                                    </h2>
                                    <div class="post-meta">
                                        <span class="post-author"><a href="#">John Doe</a></span>
                                        <span class="post-date">Mar 31, 2017</span>
                                    </div>
                                    <p>Pityful a rethoric question ran over her cheek When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of he...</p>
                                </div>
                                Post content end
                            </div>
                            Post Block style 6 end
                            <div class="gap-30"></div>
                            <div class="post-block-style post-float-half clearfix">
                                <div class="post-thumb">
                                    <a href="#">
                                    <img class="img-fluid" src="public/frontend/images/news/lifestyle/architecture1.jpg" alt="" />
                                    </a>
                                </div>
                                <a class="post-cat" href="#">Architecture</a>
                                <div class="post-content">
                                    <h2 class="post-title">
                                        <a href="#">Science meets architecture in robotically woven, solar...</a>
                                    </h2>
                                    <div class="post-meta">
                                        <span class="post-author"><a href="#">John Doe</a></span>
                                        <span class="post-date">Mar 23, 2017</span>
                                    </div>
                                    <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and ...</p>
                                </div>
                                Post content end
                            </div>
                            Post Block style 7 end
                            <div class="gap-30"></div>
                            <div class="post-block-style post-float-half clearfix">
                                <div class="post-thumb">
                                    <a href="#">
                                    <img class="img-fluid" src="public/frontend/images/news/tech/game1.jpg" alt="" />
                                    </a>
                                </div>
                                <a class="post-cat" href="#">Robotics</a>
                                <div class="post-content">
                                    <h2 class="post-title">
                                        <a href="#">Historical heroes and robot dinosaurs: New games on our…</a>
                                    </h2>
                                    <div class="post-meta">
                                        <span class="post-author"><a href="#">John Doe</a></span>
                                        <span class="post-date">Feb 24, 2017</span>
                                    </div>
                                    <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and ...</p>
                                </div>
                                Post content end
                            </div>
                            Post Block style 8 end
                        </div>
                        Item 2 end
                    </div>
                    More news carousel end
                </div>
                More news block end
            </div>
            Content Col end
            <div class="col-lg-4 col-md-12">
                <div class="sidebar sidebar-right">
                    <div class="widget color-default">
                        <h3 class="block-title"><span>Latest Reviews</span></h3>
                        <div class="list-post-block">
                            <ul class="list-post review-post-list">
                                <li class="clearfix">
                                    <div class="post-block-style post-float clearfix">
                                        <div class="post-thumb">
                                            <a href="#">
                                            <img class="img-fluid" src="public/frontend/images/news/review/review1.jpg" alt="" />
                                            </a>
                                        </div>
                                        Post thumb end
                                        <div class="post-content">
                                            <h2 class="post-title">
                                                <a href="#">Topical Resorts you need to know</a>
                                            </h2>
                                            <div class="post-meta">
                                                <div class="review-stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                            Post meta end
                                        </div>
                                        Post content end
                                    </div>
                                    Post block style end
                                </li>
                                Li 1 end
                                <li class="clearfix">
                                    <div class="post-block-style post-float clearfix">
                                        <div class="post-thumb">
                                            <a href="#">
                                            <img class="img-fluid" src="public/frontend/images/news/review/review2.jpg" alt="" />
                                            </a>
                                        </div>
                                        Post thumb end
                                        <div class="post-content">
                                            <h2 class="post-title">
                                                <a href="#">Apple - MacBook Pro with Retina display</a>
                                            </h2>
                                            <div class="post-meta">
                                                <div class="review-stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        Post content end
                                    </div>
                                    Post block style end
                                </li>
                                Li 2 end
                                <li class="clearfix">
                                    <div class="post-block-style post-float clearfix">
                                        <div class="post-thumb">
                                            <a href="#">
                                            <img class="img-fluid" src="public/frontend/images/news/review/review3.jpg" alt="" />
                                            </a>
                                        </div>
                                        Post thumb end
                                        <div class="post-content">
                                            <h2 class="post-title">
                                                <a href="#">Asus ZenPad 3S 10 Z500M</a>
                                            </h2>
                                            <div class="post-meta">
                                                <div class="review-stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        Post content end
                                    </div>
                                    Post block style end
                                </li>
                                Li 3 end
                                <li class="clearfix">
                                    <div class="post-block-style post-float clearfix">
                                        <div class="post-thumb">
                                            <a href="#">
                                            <img class="img-fluid" src="public/frontend/images/news/review/review4.jpg" alt="" />
                                            </a>
                                        </div>
                                        Post thumb end
                                        <div class="post-content">
                                            <h2 class="post-title">
                                                <a href="#">Polar M600 GPS Smart Sports Watch</a>
                                            </h2>
                                            <div class="post-meta">
                                                <div class="review-stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        Post content end
                                    </div>
                                    Post block style end
                                </li>
                                Li 4 end
                            </ul>
                            List post end
                        </div>
                        List post block end
                    </div>
                    Latest Review Widget end
                    <div class="widget m-bottom-0">
                        <h3 class="block-title"><span>Newsletter</span></h3>
                        <div class="ts-newsletter">
                            <div class="newsletter-introtext">
                                <h4>Get Updates</h4>
                                <p>Subscribe our newsletter to get the best stories into your inbox!</p>
                            </div>
                            <div class="newsletter-form">
                                <form action="#" method="post">
                                    <div class="form-group">
                                        <input type="email" name="email" id="newsletter-form-email" class="form-control form-control-lg" placeholder="E-mail" autocomplete="off">
                                        <button class="btn btn-primary">Subscribe</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        Newsletter end
                    </div>
                    Newsletter widget end
                </div>
                Sidebar right end
            </div>
            Sidebar col end
        </div>
        Row end
    </div>
    Container end
</section> -->
<!-- 3rd block end -->
<!-- <section class="ad-content-area text-center">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <img class="img-fluid" src="public/frontend/images/banner-ads/ad-content-two.png" alt="" />
            </div>
            Col end
        </div>
        Row end
    </div>
    Container end
</section> -->
<!-- Ad content two end -->

@stop