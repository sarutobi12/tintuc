@extends('home.master2')
@section('content')
<div class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(isset($key))
                <h2>Kết quả tìm kiếm : {{ $key}}</h2>
                @else
                <h2>Các tin tức với tags : {{ $tags}}</h2>
                @endif
            </div>
            <!-- Col end -->
        </div>
        <!-- Row end -->
    </div>
    <!-- Container end -->
</div>
<!-- Page title end -->
<section class="block-wrapper">
    <div class="container">
        <div class="row">

            <div class="col-lg-8 col-md-12">
                <div class="block category-listing category-style2">
                    <h3 class="block-title"><span></span></h3>
                    <?php $i=0; ?>
                    @foreach($news_serch as $item_serch)
                    <div class="post-block-style post-list clearfix">
                        <div class="row">
                            <div class="col-lg-5 col-md-6">
                                <div class="post-thumb thumb-float-style">
                                    <a href="{{url('chi-tiet/'.$item_serch->slug)}}">
                                        <img class="img-fluid" src="{{url('/public/img/news/100x100/'.$item_serch->newimg)}}" alt="" />
                                    </a>
                                    <a class="post-cat" href="#">Đô thị</a>
                                </div>
                            </div>
                            <!-- Img thumb col end -->
                            <div class="col-lg-7 col-md-6">
                                <div class="post-content">
                                    <h2 class="post-title title-large">
                                        <a href="{{url('chi-tiet/'.$item_serch['slug'])}}">{{$item_serch['newsname']}}</a>
                                    </h2>
                                    <div class="post-meta">
                                        <span class="post-author"><a href="#">{{$item_serch->newuser}}</a></span>
                                        <span class="post-date">{{ $item_serch->created_at }}</span>
                                        <span data-href="{{url()->current()}} class="post-comment pull-right"> <i class="fa fa-comments-o"></i>
                                            <a href="#" class="comments-link"><span>03</span></a></span>
                                        </div>
                                        <p>{!! $item_serch['newintro'] !!}</p>
                                    </div>
                                    <!-- Post content end -->
                                </div>
                                <!-- Post col end -->
                            </div>
                            <!-- 1st row end -->
                        </div>
                        <!-- 1st Post list end -->

                    </div>
                    @endforeach
                    <!-- Block Technology end -->
                    <div class="paging">
                        <ul class="pagination">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">»</a></li>
                            <li>
                                <span class="page-numbers">Page 1 of 2</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Content Col end -->
                @include('home.sitebar_right2')
                <!-- Sidebar Col end -->
            </div>
            <!-- Row end -->
        </div>
        <!-- Container end -->
    </section>
    <!-- First block end -->
    <!-- First block end -->
    @stop