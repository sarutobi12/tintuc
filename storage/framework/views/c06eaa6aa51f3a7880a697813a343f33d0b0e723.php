<?php $__env->startSection('content'); ?>
<div class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <strong class="category-link">
                        <?php if($itemnews->idlistnew !=""): ?>
                        Danh mục : <a href="<?php echo e(url('loai-tin/'.$itemnews->list_name($itemnews->idlistnew)['slug'])); ?>"><?php echo e($itemnews->list_name($itemnews->idlistnew)['listname']); ?></a>
                        <?php elseif($itemnews->idmodnew !=""): ?>
                        Danh mục : <a href="<?php echo e(url('loai-tin/'.$itemnews->mod_name($itemnews->idmodnew)['slug'])); ?>"><?php echo e($itemnews->mod_name($itemnews->idmodnew)['modname']); ?></a>
                        <?php endif; ?>
                    </strong>
                </ol>
            </div>
            <!-- Col end -->
        </div>
        <!-- Row end -->
    </div>
    <!-- Container end -->
</div>
<!-- Page title end -->
<section class="block-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="single-post">
                    <div class="post-media post-featured-image">
                        <img src="frontend/images/news/lifestyle/food1.jpg" class="img-fluid" alt="">
                    </div>
                    <div class="post-title-area">
                        <a class="post-cat" href="#"><?php echo e($itemnews->list_name($itemnews->idlistnew)['listname']); ?></a>
                        <h2 class="post-title">
                            <?php echo e($itemnews->newsname); ?>

                        </h2>
                        <div class="post-meta">
                            <span class="post-author">
                            By <a href="#"><?php echo e($itemnews->newuser); ?></a>
                            </span>
                            <span class="post-date"><i class="fa fa-clock-o"></i> <?php echo e($itemnews->created_at); ?></span>
                            <span class="post-hits"><i class="fa fa-eye"></i> <?php echo e($itemnews->view_count); ?> Lượt xem</span>
                            <span data-href="<?php echo e(url()->current()); ?>" class="post-comment"><i class="fa fa-comments-o">Bình Luận</i>
                        </div>
                    </div>
                    <!-- Post title end -->
                    <div class="post-content-area">
                        <div class="entry-content">
                            <p><?php echo $itemnews->newcontent; ?></p>

                        </div>
                        <!-- Entery content end -->
                        
                        <div class="share-items clearfix">
                            <ul class="post-social-icons unstyled">
                                <li class="facebook">
                                    <a href="<?php echo e(url()->current()); ?>" >
                                    <i class="fa fa-facebook"></i> <span class="ts-social-title">Facebook</span></a>
                                </li>
                                <li class="twitter">
                                    <a href="#">
                                    <i class="fa fa-twitter"></i> <span class="ts-social-title">Twitter</span></a>
                                </li>
                                <li class="gplus">
                                    <a href="<?php echo e(url()->current()); ?>">
                                    <i class="fa fa-google-plus"></i> <span class="ts-social-title">Google +</span></a>
                                </li>
                                <li class="pinterest">
                                    <a href="#">
                                    <i class="fa fa-pinterest"></i> <span class="ts-social-title">Pinterest</span></a>
                                </li>
                            </ul>
                        </div>
                        <!-- Share items end -->
                    </div>
                    <!-- post-content end -->
                </div>
                <!-- Single post end -->
                <nav class="post-navigation clearfix">                   
                    <div class="post-previous">
                        <a href="#">
                            <span><i class="fa fa-angle-left"></i>Quay lại bài trước</span>
                            <h3>
                                Deleting fears from the brain means you might never need to face them
                            </h3>
                        </a>
                    </div>
                    <div class="post-next">
                        <a href="#">
                            <span>Đọc bài tiếp theo <i class="fa fa-angle-right"></i></span>
                            <h3>
                                Smart packs parking sensor tech and beeps when collisions
                            </h3>
                        </a>
                    </div>
                    
                </nav>
                <!-- Post navigation end -->
                <div class="author-box">
                    <div class="author-img pull-left">
                        <img src="frontend/images/news/author.png" alt="">
                    </div>
                    <div class="author-info">
                        <h3>Elina Themen</h3>
                        <p class="author-url"><a href="#">http://www.newsdaily247.com</a></p>
                        <p>Selfies labore, leggings cupidatat sunt taxidermy umami fanny pack typewriter hoodie art party voluptate. Listicle meditation paleo, drinking vinegar sint direct trade.</p>
                        <div class="authors-social">
                            <span>Follow Me: </span>
                            <a href="#"><i class="fa fa-behance"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        </div>
                    </div>
                </div>
                <!-- Author box end -->
                
               
                <!-- Comments form end -->
            </div>
            <!-- Content Col end -->
            
                <?php echo $__env->make('home.sitebar_right2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- Sidebar right end -->
            
            <!-- Sidebar Col end -->
        </div>
        <!-- Row end -->
    </div>
    <!-- Container end -->
</section>
<!-- First block end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>