<div id="top-bar" class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="ts-date">
                            <i class="fa fa-calendar-check-o"></i>Tháng 3, 2019
                        </div>
                        <ul class="unstyled top-nav">
                            <li><a href="#">giới thiệu</a></li>
                            <li><a href="#">Viết về chúng tôi</a></li>
                            <li><a href="#">Quảng cáo</a></li>
                            <li><a href="#">Liên hệ</a></li>
                        </ul>
                    </div><!--/ Top bar left end -->

                    <div class="col-md-4 top-social text-lg-right text-md-center">
                        <ul class="unstyled">
                            <li>
                                <a title="Facebook" href="#">
                                    <span class="social-icon"><i class="fa fa-facebook"></i></span>
                                </a>
                                <a title="Twitter" href="#">
                                    <span class="social-icon"><i class="fa fa-twitter"></i></span>
                                </a>
                                <a title="Google+" href="#">
                                    <span class="social-icon"><i class="fa fa-google-plus"></i></span>
                                </a>
                                <a title="Linkdin" href="#">
                                    <span class="social-icon"><i class="fa fa-linkedin"></i></span>
                                </a>
                                <a title="Rss" href="#">
                                    <span class="social-icon"><i class="fa fa-rss"></i></span>
                                </a>
                                <a title="Skype" href="#">
                                    <span class="social-icon"><i class="fa fa-skype"></i></span>
                                </a>
                            </li>
                        </ul><!-- Ul end -->
                    </div><!--/ Top social col end -->
                </div><!--/ Content row end -->
            </div><!--/ Container end -->
</div><!--/ Topbar end -->
<!-- Header start -->
<header id="header" class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="logo">
                            <a href="index.html">
                                <img src="public/frontend/images/logos/logo.png" alt="">
                            </a>
                        </div>
                    </div><!-- logo col end -->

                    <div class="col-md-9 col-sm-12 header-right">
                        <div class="ad-banner float-right">
                            <a href="#"><img src="public/frontend/images/banner-ads/ad-top-header.png" class="img-fluid" alt=""></a>
                        </div>
                    </div><!-- header right end -->
                </div><!-- Row end -->
            </div><!-- Logo and banner area end -->
</header>
<!--/ Header end -->
<div class="main-nav clearfix">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-lg col">
                <div class="site-nav-inner float-left">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <!-- End of Navbar toggler -->
                    <div id="navbarSupportedContent" class="collapse navbar-collapse navbar-responsive-collapse">
                        <ul class="nav navbar-nav">
                            <li class="nav-item dropdown active">
                                <a href="<?php echo e(url('')); ?>" class="nav-link">Trang Chủ <i class=""></i></a>
                            </li>
                            <?php $__currentLoopData = $modnews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $itemmod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="dropdown">
                                <a href="<?php echo e(url('loai-tin').'/'.$itemmod->slug); ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo e($itemmod->modname); ?> <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <?php $__currentLoopData = $itemmod->listnews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemlist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                    <li class="dropdown-submenu">
                                        <a href="<?php echo e(url('loai-tin').'/'.$itemlist->slug); ?>"><?php echo e($itemlist->listname); ?></a>
                                    </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul><!-- End dropdown -->
                            </li>
                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                            <!-- Tab menu end -->
                            <!-- Features menu end -->
                        </ul>
                        <!--/ Nav ul end -->
                    </div>
                    <!--/ Collapse end -->
                </div>
                <!-- Site Navbar inner end -->
            </nav>
            <!--/ Navigation end -->
            <div class="nav-search">
                <span id="search"><i class="fa fa-search"></i></span>
            </div>
            <!-- Search end -->
            <div class="search-block" style="display: none;">
                <form action="<?php echo e(url('/search2')); ?>">
                <input type="text" name="key" type="text" class="form-control" placeholder="Nhập từ khóa tìm kiếm">
                <span class="search-close">&times;</span>
                </form>
            </div>
            <!-- Site search end -->
        </div>
        <!--/ Row end -->
    </div>
    <!--/ Container end -->
</div>