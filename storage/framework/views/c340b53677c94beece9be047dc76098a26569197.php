<div class="col-lg-4 col-md-12">
    <div class="sidebar sidebar-right">
        <div class="widget">
            <h3 class="block-title"><span>Theo dõi chúng tôi</span></h3>
            <ul class="social-icon">
                <li><a href="#" target="_blank"><i class="fa fa-rss"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li>
            </ul>
        </div>
        <!-- Widget Social end -->
        <div class="widget color-default">
            <h3 class="block-title"><span>Tin mới nhất</span></h3>
            <?php $count =0; ?>
            <?php $__currentLoopData = $lasted_news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item_lt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($count <5): ?>
            <div class="list-post-block">
                <ul class="list-post">
                    <li class="clearfix">
                        <div class="post-block-style post-float clearfix">
                            <div class="post-thumb">
                                <a href="<?php echo e(url('chi-tiet/'.$item_lt->slug)); ?>">
                                    <img class="img-fluid" src="<?php echo e(url('/public/img/news/100x100/'.$item_lt->newimg)); ?>" alt="" />
                                </a>
                                <a class="post-cat" href="<?php echo e(url('chi-tiet/'.$item_lt->slug)); ?>">Hot</a>
                            </div>
                            <!-- Post thumb end -->
                            <div class="post-content">
                                <h2 class="post-title title-small">
                                    <a href="<?php echo e(url('chi-tiet/'.$item_lt->slug)); ?>"><?php echo e($item_lt->newsname); ?></a>
                                </h2>
                                <div class="post-meta">
                                    <span class="post-date"><?php echo e($item_lt->created_at); ?></span>
                                </div>
                            </div>
                            <!-- Post content end -->
                        </div>
                        <!-- Post block style end -->
                    </li>
                    <!-- Li 4 end -->
                </ul>
                <!-- List post end -->
            </div>
            <?php endif; ?>
            <?php  $count = $count +1; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <!-- List post block end -->
        </div>
        <!-- Popular news widget end -->
        <div class="widget text-center">
           <?php if($adverts_bottom[0]->code != ""): ?>
           <?php echo e($adverts_bottom[0]->code); ?>

           <?php else: ?>
           <a href="<?php echo e($adverts_bottom[0]->link); ?>">
              <img src="<?php echo e(url('public/img/images_bn/'.$adverts_bottom[0]->img)); ?>" alt="No image" />
          </a>
          <?php endif; ?>
      </div>
      <!-- Sidebar Ad end -->
      <div class="widget color-default m-bottom-0">
        <h3 class="block-title"><span>Đọc nhiều nhất</span></h3>
        <div id="post-slide" class="owl-carousel owl-theme post-slide">
           <?php $count =1; ?>
           <?php $__currentLoopData = $most_news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item_most): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           <?php if($count <6): ?>
           <div class="item">
            <div class="post-overaly-style text-center clearfix">
                <div class="post-thumb">
                    <a href="<?php echo e(url('chi-tiet/'.$item_most->slug)); ?>">
                        <img class="img-fluid" src="<?php echo e(url('/public/img/news/100x100/'.$item_most->newimg)); ?>" alt="" />
                    </a>
                </div>
                <!-- Post thumb end -->
                <div class="post-content">
                    <a class="post-cat" href="#">Gadgets</a>
                    <h2 class="post-title">
                        <a href="<?php echo e(url('chi-tiet/'.$item_most->slug)); ?>"><?php echo e($item_most->newsname); ?></a>
                    </h2>
                    <div class="post-meta">
                        <span class="post-date"><?php echo e($item_most->created_at); ?></span>
                    </div>
                </div>
                <!-- Post content end -->
            </div>
        </div>
        <?php endif; ?>
        <?php  $count = $count +1; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <!-- Item 2 end -->
    </div>
    <!-- Post slide carousel end -->
</div>
<hr>
<div class="widget text-center">
   <?php if($adverts_bottom[1]->code != ""): ?>
   <?php echo e($adverts_bottom[1]->code); ?>

   <?php else: ?>
   <a href="<?php echo e($adverts_bottom[1]->link); ?>">
      <img src="<?php echo e(url('public/img/images_bn/'.$adverts_bottom[1]->img)); ?>" alt="No image" />
  </a>
  <?php endif; ?>
</div>
<div class="widget text-center">
   <?php if($adverts_bottom[0]->code != ""): ?>
   <?php echo e($adverts_bottom[0]->code); ?>

   <?php else: ?>
   <a href="<?php echo e($adverts_bottom[0]->link); ?>">
      <img src="<?php echo e(url('public/img/images_bn/'.$adverts_bottom[0]->img)); ?>" alt="No image" />
  </a>
  <?php endif; ?>
</div>

<!-- Trending news end -->
</div>
<!-- Sidebar right end -->
</div>
            <!-- Sidebar Col end -->